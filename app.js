/* События клавиатуры предназначены именно для работы с клавиатурой.
Их можно использовать для проверки ввода в < input >, но будут недочёты, например, текст может быть вставлен мышкой,
    при помощи правого клика и меню, без нажатия клавиши, и тут уже нам события клавиатуры не помогут.
 */

const btn = document.querySelectorAll('.btn')

document.addEventListener('keydown', (e) => {
    btn.forEach(el => {
        if (e.code === el.dataset.key) {
            el.classList.add('active')
        } else { el.classList.remove('active') }
    })
})


